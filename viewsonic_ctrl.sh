#!/bin/bash

PROJECTOR_CMD="${1:-__help__}"

CLR_RED="\e[31m"
CLR_GRN="\e[32m"
CLR_YEL="\e[33m"
CLR_CYA="\e[36m"
CLR_END="\e[0m"

TTY_LIST=$(ls /dev | grep ttyUSB)

function setup_all() {
  for port in $TTY_LIST
    do
      echo "Configure:  $port"
      #stty -F "/dev/$port" raw speed 115200
      stty -F "/dev/$port" 115200 cs8 -cstopb -parenb
    done
}

if [ "zzz$TTY_LIST" = "zzz" ]; then
  echo "Not found ttyUSB devices, check connection!"
#else
#  echo -e "Found:\n$TTY_LIST"
#  setup_all
fi

function send_data() {
  # $1 - data
  # $2 - tty device
  echo -ne $1 > $2
}

function send_for_all() {
  # $1 - data
  for port in $TTY_LIST
    do
      echo "Send:  $1   to   $port"
      send_data $1 "/dev/$port"
    done
}

function disable_3d() {
  send_for_all '\x06\x14\x00\x04\x00\x34\x12\x20\x00\x7E'
}

function enable_3d() {
  send_for_all '\x06\x14\x00\x04\x00\x34\x12\x20\x05\x83'
}

function power_on() {
  send_for_all '\x06\x14\x00\x04\x00\x34\x11\x00\x00\x5D'
}

function power_off() {
  send_for_all '\x06\x14\x00\x04\x00\x34\x11\x01\x00\x5E'
}

function help() {
  echo -e "${CLR_RED}WARNING: ${CLR_END}This script will configure and send binary data to every ${CLR_YEL}/dev/ttyUSB<n>${CLR_END}"
  echo "device, please disconnect all unnecessary USB tty adapters before usage"
  echo -e "Usage:"
  echo -e "\t./viewsonic_ctrl.sh <command>\n"
  echo "COMMANDS:"
  echo -e "\t on - send command to power on peojectors"
  echo -e "\t off - send command to power off peojectors"
  echo -e "\t enable3d - enable 3d function on projectors"
  echo -e "\t disable3d - disable 3d function on projectors"
}

if [ "$PROJECTOR_CMD" = '__help__' ]; then
    help
    exit 0
fi

setup_all

if [ "$PROJECTOR_CMD" = 'on' ]; then
    power_on
    echo -e "\t\t${CLR_GRN}POWER ON${CLR_END}"
elif [ "$PROJECTOR_CMD" = 'off' ]; then
    power_off
    echo -e "\t\t${CLR_RED}POWER OFF${CLR_END}"
elif [ "$PROJECTOR_CMD" = 'enable3d' ]; then
    enable_3d
    echo -e "\t\t${CLR_CYA}3D ON${CLR_END}"
elif [ "$PROJECTOR_CMD" = 'disable3d' ]; then
    disable_3d
    echo -e "\t\t${CLR_RED}3D OFF${CLR_END}"
else
    help
fi
